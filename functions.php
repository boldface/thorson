<?php

/**
 * @package Boldface/Thorson
 */
declare( strict_types = 1 );
namespace Boldface\Thorson;

/**
 * Add updater controller to the admin_init hook
 *
 * @since 1.0
 */
function admin_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->admin_init();
}
\add_action( 'admin_init', __NAMESPACE__ . '\admin_init' );

/**
 * Filter the modules list.
 *
 * @since 1.0
 *
 * @param array $list List of modules.
 *
 * @return array The new list of modules.
 */
function controllersList( array $list ) : array {
  $list[] = 'featuredImage';
  $list[] = 'images';
  $list[] = 'openGraph';
  $list[] = 'googleFonts';
  $list[] = 'gallery';
  $list[] = 'widgets';
  return $list;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\controllersList' );

/**
 * Add the call-to-action sidebar to the sidebars array.
 *
 * @since 1.0
 *
 * @param array $list List of sidebars.
 *
 * @return array The new list of sidebars.
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'id' => 'call-to-action',
    'name' => 'Call to Action',
    'hook' => 'Boldface\Bootstrap\Views\loop\start',
    'before_widget' => '',
    'after_widget' => '',
  ];
  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 * Only display the call-to-action widget on the front page.
 *
 * @since 1.0
 *
 * @param bool   $bool Whether to short-circuit the widget. Unused.
 *
 * @return bool Whether to short-circuit displaying the widget.
 */
function widgets( bool $bool ) : bool {
  return ! \is_front_page();
}
\add_filter( 'Boldface\Bootstrap\Views\widgets\call-to-action', __NAMESPACE__ . '\widgets' );

/**
 * Return the new gallery class.
 *
 * @since 1.0
 *
 * @param string $class The old gallery class.
 *
 * @return string The new gallery class.
 */
function galleryClass( string $class ) : string {
  if( \is_front_page() ) {
    $class = str_replace( 'flex-wrap', 'flex-md-nowrap flex-wrap', $class );
  }
  return str_replace( 'flex-wrap', 'flex-wrap justify-content-center', $class );
}
\add_filter( 'Boldface\Bootstrap\Views\gallery\class', __NAMESPACE__ . '\galleryClass', 11 );

/**
 * Return the new button class.
 *
 * @since 1.0
 *
 * @param string $class The old button class.
 *
 * @return string The new button class.
 */
function buttonClass( string $class ) : string {
  return str_replace( 'btn-primary', 'btn-secondary', $class );
}
\add_filter( 'Boldface\Bootstrap\Models\contactForm7\button\class', __NAMESPACE__ . '\buttonClass' );

/**
 * Use the wp_enqueue_scripts hook to enqueue the thorson stylesheet.
 *
 * @since 1.0
 */
function enqueue_scripts() {
  \wp_enqueue_style( 'thorson', \esc_url( \get_stylesheet_directory_uri() . '/assets/css/style.css' ), [ 'booter', 'bootstrap' ] );
  if( \is_front_page() ) {
    \wp_enqueue_script( 'thorson', \esc_url( \get_stylesheet_directory_uri() . '/assets/js/background.js' ), [ 'jquery' ] );
  }
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_scripts' );

/**
 * Return the new footer class.
 *
 * @since 1.0
 *
 * @param string $class The old footer class.
 *
 * @return string The new footer class.
 */
function footer_class( string $class ) : string {
  return 'footer navbar bg-thorson-nav';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footer_class' );

/**
 * Return the new footer text.
 *
 * @since 1.0
 *
 * @param string $class The old footer text.
 *
 * @return string The new footer text.
 */
function footerText( string $text ) : string {
  return sprintf( '<div class="text-white text-center">9090 Ridgeline Blvd. | Suite 230 | Highlands Ranch, CO 80129 | 303.773.6300 | fax 303.773.6302</div>' );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Return the new navigation class.
 *
 * @since 1.0
 *
 * @param string $class The old navigation class.
 *
 * @return string The new navigation class.
 */
function navigation_class( string $class ) : string {
  return 'navbar navbar-expand-md navbar-dark bg-thorson-nav fixed-top';
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigation_class' );

/**
 * Return the navigation menu arguments.
 *
 * @since 1.0
 *
 * @param array $class The old navigation menu arguments.
 *
 * @return array The new navigation menu arguments.
 */
function navMenuArgs( array $args ) : array {
  $args[ 'container_class' ] = 'collapse navbar-collapse justify-content-center';
  $args[ 'menu_class' ] = 'navbar-nav';
  return $args;
}
\add_filter( 'Boldface\Bootstrap\Models\navigation\menu\args', __NAMESPACE__ . '\navMenuArgs' );

/**
 * Return the magic constant __FUNCTION__.
 *
 * @since 1.0
 *
 * @param mixed $unused An unused variable.
 *
 * @return string The magic constant __FUNCTION__.
 */
function noImage( $unused ) : string {
  return __FUNCTION__;
}
\add_filter( 'Boldface\Bootstrap\Controllers\featuredImage\callback', __NAMESPACE__ . '\noImage' );

/**
 * Use the wp_head hook to add the post thumbnail url as a background image.
 *
 * @since 1.0
 */
function wp_head() {
  if( ! \has_post_thumbnail() ) return;

  $image = \get_the_post_thumbnail_url();
  \wp_add_inline_style( 'thorson', "article.has-post-thumbnail{background-image:URL($image);}" );
}
\add_action( 'wp_head', __NAMESPACE__ . '\wp_head' );

/**
 * Return the background-image div appended to the content on the front page.
 *
 * @since 1.0
 *
 * @param string The old content.
 *
 * @return string The new content.
 */
function loop( string $content ) : string {
  if( ! \is_front_page() ) return $content;

  return '<div id="background-image"></div>' . $content;
}
\add_filter( 'Boldface\Bootstrap\Views\entry', __NAMESPACE__ . '\loop', 20 );

/**
 * Return the new loop class.
 *
 * @since 1.0
 *
 * @param string $class The old loop class.
 *
 * @return string The new loop class.
 */
function loopClass( string $class ) : string {
  return 'container-fluid';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Return the new entry content class.
 *
 * @since 1.0
 *
 * @param string $class The old entry content class.
 *
 * @return string The new entry content class.
 */
function entryContentClass( string $class ) : string {
  $class = str_replace( 'container-fluid', '', $class );
  return trim( $class . ' container' );
}
\add_filter( 'Boldface\Bootstrap\Views\entry\content\class', __NAMESPACE__ . '\entryContentClass' );

/**
 * Return the new entry heading class.
 *
 * @since 1.0
 *
 * @param string $class The old entry heading class.
 *
 * @return string The new entry heading class.
 */
function entryHeadingClass( string $class ) : string {
  return trim( $class . ' text-white' );
}
\add_filter( 'Boldface\Bootstrap\Views\entry\heading\class', __NAMESPACE__ . '\entryHeadingClass' );

/**
 * Return the new Google Fonts.
 *
 * @since 1.0
 *
 * @param array $fonts The old Google Fonts.
 *
 * @return array The new Google Fonts.
 */
function googleFonts( array $fonts ) : array {
  return [ 'Lato', 'Source+Sans+Pro' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Return the new login logo.
 *
 * @since 1.0
 *
 * @param array $logo The old login logo.
 *
 * @return array The new login logo.
 */
function loginLogo( string $logo ) : string {
  return \esc_url( \get_stylesheet_directory_uri() . '/assets/images/logo.png' );
}
\add_filter( 'Boldface\Bootstrap\settings\loginLogo', __NAMESPACE__ . '\loginLogo' );

/**
 * Return the new login background.
 *
 * @since 1.0
 *
 * @param array $logo The old login background.
 *
 * @return array The new login background.
 */
function backgroundImage( string $background ) : string {
  return \esc_url( \get_stylesheet_directory_uri() . '/screenshot.png' );
}
\add_filter( 'Boldface\Bootstrap\settings\backgroundImage', __NAMESPACE__ . '\backgroundImage' );

/**
 * Add filter to bypass the Bootstrap REST API.
 *
 * @since 1.0
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );

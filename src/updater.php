<?php

/**
 * @package Boldface/Thorson
 */
declare( strict_types = 1 );
namespace Boldface\Thorson;

/**
 * Class for updating the theme
 *
 * @since 1.0
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'thorson';
  protected $repository = 'boldface/thorson';
}

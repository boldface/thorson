<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFunctions extends \WP_UnitTestCase {

  function testcontrollersList() {
    $controllersList = \Boldface\Thorson\controllersList([]);
    $this->assertSame( [  'featuredImage', 'images', 'openGraph' ], $controllersList );
  }

  function testFooterClass() {
    $footerClass = \Boldface\Thorson\footer_class('');
    $this->assertSame( 'footer navbar bg-thorson-gold', $footerClass );
  }

  function testNavigationClass() {
    $navigationClass = \Boldface\Thorson\navigation_class('');
    $this->assertSame( 'navbar navbar-expand-md navbar-dark bg-thorson-grey', $navigationClass );
  }

  function testLoopClass() {
    $loopClass = \Boldface\Thorson\loopClass('');
    $this->assertSame( 'container-fluid', $loopClass );
  }

  function testEntryContentClass() {
    $entryContentClass = \Boldface\Thorson\entryContentClass('');
    $this->assertSame( 'container', $entryContentClass );
  }

  function testEntryContentClassTest() {
    $entryContentClass = \Boldface\Thorson\entryContentClass('test');
    $this->assertSame( 'test container', $entryContentClass );
  }

  function testEntryHeadingClass() {
    $entryHeadingClass = \Boldface\Thorson\entryHeadingClass('');
    $this->assertSame( 'thorson-red border rounded border-thorson-grey', $entryHeadingClass );
  }

  function testEntryHeadingClassTest() {
    $entryHeadingClass = \Boldface\Thorson\entryHeadingClass('test');
    $this->assertSame( 'test thorson-red border rounded border-thorson-grey', $entryHeadingClass );
  }

  function testNoImage() {
    $noImage = \Boldface\Thorson\noImage('');
    $this->assertSame( 'Boldface\Thorson\noImage', $noImage );
  }

}
